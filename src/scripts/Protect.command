IFS=$'\n'

if [ "${DEPLOYMENT_LOCATION}" == "YES" ]; then
    PREFIX=${BUILT_PRODUCTS_DIR}/..
else
    PREFIX=${BUILT_PRODUCTS_DIR}
fi

cd ${PREFIX}${PUBLIC_HEADERS_FOLDER_PATH}

chmod a-w *.h 2>> /dev/null
chmod a-w private/*.h 2>> /dev/null

exit 0
